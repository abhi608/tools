% SSH protocol
% Piyush P Kurur
% Sep 8, 2016

# Secure Shell protocol.

## As a replacement of telnet

- Log in to a remote machine

- Run a command on the remote machine


## Authentication

- Authentication by password based access

- Public/Private key authentication.

- Kerberos and other methods.

## Password based access

- Password is sent to the machine.

- Usual password authentication is done.

- One secret, cannot share an account etc.

## Key based access

- User creates a public key private key pair.

- The public key is deposited at the server

- No password is ever sent

- very secure.


## Setting up the key


1. Generate your key using the `ssh-keygen` command

        ssh-keygen

2. Copy the public key

        ssh ppk@turing 'cat >> .ssh/authorized_keys' < .ssh/id_rsa.pub

3. Keep the corresponding key secret (do not transfer it over network)

4. There is no shame in public-key (you can flout it as you want)

5. Set it up on multiple machines.



## SSH agents (type passphrase only once)

- Runs in the background and authenticates

```bash

$ ssh-agent bash
$ ssh-add
$ ssh ppk@hell.org

```

## Using ssh-agent with xinit

```bash

$ cat .xinitrc

#!/bin/bash
/usr/bin/env ssh-agent /usr/bin/xmonad

```

## Port forwarding


```bash
$ ssh -L 8000:smtp.cse.iitk.ac.in:25 username@hell.com
```

. . .

~~~

laptop:8000 ---> hell.com  # over ssh
            ---> smpt.cse.iitk.ac.in:25

~~~

The connection from login.cse.iitk.ac.in is **not* encrypted.

## SSH protocol

1. SSH transport layer (actual encryption stuff)

2. SSH authentication layer

3. SSH connection layer


## SSH Transport layer

- Establish a secure reliable channel of communication

- Negotiate encryption key and other stuff.

- Checking hostkey


## SSH authentication layer

- Authenticates user

- many possible authentication methods `password` `publickey`

- client driven

## Connection layer

- use the single connection to multiplex data

- Open a _channel_ of some _type_

- communicates data on the canned dependent on the type.

## Some  common channel

- open `session` - interactive session

- requests `"ptr-req"` `"x11"`

- channel specific data.

## Shameless advertisement


Want to implement SSH from scratch ?

<https://github.com/raaz-crypto/raaz-ssh>
