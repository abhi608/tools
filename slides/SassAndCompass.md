% Styling websites using Sass and Compass.
% Piyush P Kurur
% March 9, 2015


# How to style a website?

## The main goals.

- Efficiently convey the information

- Accessible across devices (even mobiles)


## The default wisdom

- Html should not contain styling info

- CSS should be used to style.


## The three commandments.

- Thou shall not write HTML in HTML

- Thou shall not write  CSS in CSS

- Thou should never ever dream of JavaScript. It is the language of
  the Satan.


## The CSS problem

- Does not have variables,

- Cannot refactor recurring patterns.

- Cannot modularise.

## [Sass]

- [Syntactically Awesome Style Sheets][sass].

- Supports variables, nesting, mixins, imports, basic arithmetic.

- <http://sass-lang.com/>


## [Compass]: What and why?

- A "library" on top of Sass

- Mixins for a wide variety of tasks.

- Work across browsers.

## Grids and layout.

- How many columns?

- Mobile first approach

- Use [susy].

## Misc stuff.

- Icons using [Font Awesome]

- Google fonts.


## Other CSS framework

- [Twitter bootstrap]

- Advantage: Easier to getting started.

- Disadvantage: Huge, not semantic, verbose and is a mess.

[sass]: <http://sass-lang.com/>
[compass]: <http://compass-style.org/>
[susy]:<http://susy.oddbird.net/>
[font awesome]:<http://fortawesome.github.io/Font-Awesome/>
[twitter bootstrap]: <http://getbootstrap.com/}
